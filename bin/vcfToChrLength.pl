#!/usr/bin/env perl

use strict;
use warnings;
use Getopt::Long;

my $input;
my $output;
GetOptions(
	'i=s' => \$input,
	'o=s' => \$output
) or die 'wrong input argument value.';

if(!$input) {
	die 'You must provide an input vcf (-i).';
}
if(!-f$input) {
	die 'The provided input file does not exists.';
}
if(!$output) {
	die 'You must provide an output vcf path (-o).';
}

open(VCF, $input) or die 'Failed to open the input file.';
open(OUT, '>'.$output) or die 'Failed to create the output file.';

my $line = <VCF>;

# Reach the contig description lines
while($line && $line !~ /^##contig/) {
	$line = <VCF>;
}
if(!$line) {
	die 'Possible bad VCF format.';
}

while($line =~ /^##contig/ ) {
	if($line =~ /ID=([\w\.]+)\,/) {
        my $id = $1;
        $line =~ /length=(\d+)\,/;
        print OUT $id."\t".$1."\n";
    } else {
        die 'failed to identify chromosome names.';
    }
    $line = <VCF>;
}
close(VCF);
close(OUT);