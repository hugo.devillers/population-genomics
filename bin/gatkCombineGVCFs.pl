#!/usr/bin/env perl

use strict;
use warnings;
use Getopt::Long;
use threads;
use threads::shared;
use POSIX qw/ceil/;
use PopGeno::message qw(print_msg);
use PopGeno::caching qw(check_vmtouch cache_files);
use PopGeno::reference qw(check_reference);
use PopGeno::gatk qw(check_version gather combinegvcfs);
use PopGeno::intervals qw(create_intervals_by_chr);

# Init. parameter values
my $gatk;
my $threads = 4;
my $interval_dir = 'interval_lists';
my $ref_genome;
my $gvcf_fofn;
my $base_out;

# Retrieve parameter values
GetOptions(
    '-ref-genome|r=s'    => \$ref_genome,
    '-output-base|o=s'   => \$base_out,
    '-gatk-bin|g=s'      => \$gatk,
    '-interval-dir|i=s'  => \$interval_dir,
    '-gvcf-fofn|v=s'     => \$gvcf_fofn,
    '-threads|t=i'       => \$threads
) or die 'Wrong input parameter value(s).';

# Check arguments
if( !$gvcf_fofn ) {
    die 'You must provide a file that contains gvcf pathes.';
}
if( !-f$gvcf_fofn ) {
    die 'The provided file of gvcf pathes does not exists.';
}

# Check GATK path
$gatk = check_version($gatk);

# Check vmtouch tool
check_vmtouch();

# Check the reference genome
my $ref_dict = check_reference($ref_genome);

# Load the list of files to combine
my @gvcf_files;
my @files_to_cache;
open(TXT, $gvcf_fofn) or die 'Failed to open the file of gvcf pathes.';
while( my $line = <TXT> ) {
    chomp($line);
    if( !-f $line ) {
        die 'The gvcf file '.$line.' does not exists.';
    }
    push @gvcf_files, $line;
    push @files_to_cache, $line;
}

# Cache files
push @files_to_cache, $ref_genome;
push @files_to_cache, $ref_genome.'.fai';
push @files_to_cache, $ref_dict;
my @caching_threads;
cache_files(\@files_to_cache, \@caching_threads);

# While caching file, prepare intervals
create_intervals_by_chr($interval_dir, $gatk, $ref_genome, $threads);

# Wait for file caching thread
foreach (@caching_threads) {
    $_->join();
}

# Combine GVCF
my @gatk_threads;
my @gvcf :shared;
my @logs :shared;
foreach my $int (0..($threads-1)){
    my $gatk_thread = threads->new(\&combinegvcfs, $gatk, $interval_dir, $int, \@gvcf_files, $ref_genome, $base_out, \@gvcf, \@logs);
    push @gatk_threads, $gatk_thread;
}
foreach (@gatk_threads) { $_->join(); }

# Gather splitted g.vcf files
gather($gatk, \@gvcf, $base_out, 1);

# Cleaning
print_msg('Main thread', 1, 'Cleaning up temporary files...');
foreach (@gvcf) { 
    unlink $_;
    unlink $_.'.idx';
}
foreach (@logs) {
    unlink $_;
}
print_msg('Main thread', 1, 'All processes completed.');
