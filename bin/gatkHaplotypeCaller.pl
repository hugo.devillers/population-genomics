#!/usr/bin/env perl

use strict;
use warnings;
use Getopt::Long;
use threads;
use threads::shared;
use POSIX qw/ceil/;
use PopGeno::message qw(print_msg);
use PopGeno::caching qw(check_vmtouch cache_files);
use PopGeno::reference qw(check_reference);
use PopGeno::gatk qw(check_version gather haplotypecaller);
use PopGeno::intervals qw(create_intervals create_intervals_by_chr create_intervals_no_chr_cut);
use PopGeno::bam qw(check_bam);

# Init. parameter values
my $gatk;
my $threads = 4;
my $interval_dir = 'interval_lists';
my $interval_mode = 'balanced';
my $sample_ploidy = 2;
my $input_bam;
my $ref_genome;
my $base_out;
my $do_gvcf;

# Retrieve parameter values
GetOptions(
    '-bam|b=s'           => \$input_bam,
    '-ref-genome|r=s'    => \$ref_genome,
    '-output-base|o=s'   => \$base_out,
    '-gatk-bin|g=s'      => \$gatk,
    '-sample-ploidy|p=i' => \$sample_ploidy,
    '-interval-dir|i=s'  => \$interval_dir,
    '-interval-mode|m=s' => \$interval_mode,
    '-gvcf'              => \$do_gvcf,
    '-threads|t=i'       => \$threads
) or die 'Wrong input parameter value(s).';

# Check GATK path
$gatk = check_version($gatk);

# Check vmtouch tool
check_vmtouch();

# Check the reference genome
my $ref_dict = check_reference($ref_genome);

# Check the bam file
my $input_bai = check_bam($input_bam);

# MAIN
# A first thread will manage file caching
my @files_to_cache = ($ref_genome, $input_bam, $ref_genome.'.fai', $input_bai, $ref_dict);
my @caching_threads;
cache_files(\@files_to_cache, \@caching_threads);

# While caching file, prepare intervals
if($interval_mode eq 'balanced') {
	create_intervals($interval_dir, $gatk, $ref_genome, $threads, $input_bam);
} elsif($interval_mode eq 'chr') {
	create_intervals_by_chr($interval_dir, $gatk, $ref_genome, $threads);
} elsif($interval_mode eq 'balanced_chr') {
	create_intervals_no_chr_cut($interval_dir, $gatk, $ref_genome, $threads);
} else {
	die "Unknown interval division mode: $interval_mode.\n";
}

# Wait for file caching thread
foreach (@caching_threads) {
    $_->join();
}

# Launch 
my @gatk_threads;
my @gvcf :shared;
my @logs :shared;
foreach my $int (0..($threads-1)){
    my $gatk_thread = threads->new(\&haplotypecaller, $gatk, $interval_dir, $int, $input_bam, $ref_genome, $base_out, $sample_ploidy, \@gvcf, \@logs, $do_gvcf);
    push @gatk_threads, $gatk_thread;
}
foreach (@gatk_threads) { $_->join(); }

# Gather splitted g.vcf files
gather($gatk, \@gvcf, $base_out, $do_gvcf);

# Cleaning
print_msg('Main thread', 1, 'Cleaning up temporary files...');
foreach (@gvcf) { 
    unlink $_;
    unlink $_.'.idx';
}
foreach (@logs) {
    unlink $_;
}
print_msg('Main thread', 1, 'All processes completed.');
