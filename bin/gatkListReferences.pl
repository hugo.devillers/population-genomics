#!/usr/bin/env perl

use strict;
use warnings;
use Getopt::Long;
use PopGeno::vcfutils qw(get_seq_lengths);

# Init. parameter values
my $vcf;

# Retrieve parameter values
GetOptions(
    '-vcf|v=s' => \$vcf
) or die 'Wrong input argument value.';
if( !$vcf ) {
    die 'You must provide an input VCF file (-v).';
}
if( !-f$vcf ) {
    die 'The provided VCF file does not exists.';
}

# Get sample ids
my @ids;
my @lengths;
get_seq_lengths($vcf, \@ids, \@lengths);

# Print it
foreach (0..(scalar(@ids)-1)) {
    print $ids[$_]."\t".$lengths[$_]."\n";
}
