#!/usr/bin/env perl

use strict;
use warnings;
use Getopt::Long;

# ARGUMENTS
my $input;
my $output;
my $chr_to_num;
my $fix_smp_id;
my $var_prefix = "V";
GetOptions(
	'-input|i=s'      => \$input,
	'-output|o=s'     => \$output,
    '-chr-to-num|n'   => \$chr_to_num,
    '-fix-smp-id|f'   => \$fix_smp_id,
    '-var-prefix|v=s' => \$var_prefix
) or die 'wrong input argument value.';

# ARGUMENT CHECK
if(!$input) {
	die 'You must provide an input vcf (-i).';
}
if(!-f$input) {
	die 'The provided input file does not exists.';
}
if(!$output) {
	die 'You must provide an output vcf path (-o).';
}

# OPEN/CREATE FILES
open(VCF, $input) or die 'Failed to open the input file.';
open(OUT, '>'.$output) or die 'Failed to create the output file.';

# COPY THE HEADER
my $line = <VCF>;
# Reach the contig description lines
while($line && $line !~ /^##contig/) {
	print OUT $line;
	$line = <VCF>;
}
if(!$line) {
	die 'Possible bad VCF format.';
}
# Build a name converter hash
my %conv;
my $chri = 0;
if($chr_to_num) {
    while($line =~ /^##contig/ ) {
	    if($line =~ /ID=([\w\.]+)\,/) {
		    $chri++;
		    my $from = $1;
		    my $to = 'chr'.$chri;
		    $conv{$from} = $to;
		    $line =~ s/$from/$to/;
		    print OUT $line;
	    } else {
		    die 'failed to identify chromosome names.';
	    }
	    $line = <VCF>;
    }
    if($chri == 0) {
	    die 'Failed to identify chromosome names.';
    }
}
# Reach the header line
while($line && $line !~ /^#CHROM/ ) {
	print OUT $line;
	$line = <VCF>;
}
if(!$line) {
	die 'Failed to find the header line';
}
if($fix_smp_id) {
    # Some tools like PLINK fail when some characters are in smp ids
    $line =~ s/[\_\.\-]//g;
}
print OUT $line;

# PARSE THE VARIANT LINES
my $snpi = 0;
if(!$chr_to_num) {
	while($line = <VCF>) {
    	my @elems = split(/\t/, $line, 4);
    	$elems[2] = sprintf("%s%08d",$var_prefix, $snpi);
    	print OUT join("\t", @elems);
    	$snpi++;
	}
} else {
    while($line = <VCF>) {
        my @elems = split(/\t/, $line, 4);
        if(!exists($conv{$elems[0]})) {
		    die 'Undeclared chromosome names ('.$elems[0].').';
	    }
        $elems[0] = $conv{$elems[0]};
        $elems[2] = sprintf("%s%08d",$var_prefix, $snpi);
        print OUT join("\t", @elems);
        $snpi++;
    }
}
close(VCF);
close(OUT);