#!/usr/bin/env perl

use strict;
use warnings;
use Getopt::Long;
use threads;
use threads::shared;
use PopGeno::message qw(print_msg);
use PopGeno::caching qw(check_vmtouch cache_files);
use PopGeno::reference qw(check_reference);
use PopGeno::gatk qw(check_version);
use PopGeno::intervals qw(create_intervals);
use PopGeno::vcfutils qw(get_seq_lengths get_sample_ids);
use PopGeno::parse qw(parse_af_interval);

# Filter hash
my %filters = (
    'n_gap'    => 0.1,
    'min_af'   => 0.05,
    'n_min_af' => 0.8,
    'max_af'   => 0.9,
    'n_max_af' => 1
);
my %flags = (
    'gapped'   => 1,
    'n_gapped' => 2,
    'all_miaf' => 4,
    'n_miaf'   => 8,
    'n_maaf'   => 16
);

# Init. arguments
my $input;
my $ref_genome;
my $outbase;
my $ngap;
my @miaf;
my @maaf;
my $gatk;
my $interval_dir;
my $threads = 4;
my $filter = 0;
GetOptions(
	'v=s'               => \$input,
	'o=s'               => \$outbase,
    'r=s'               => \$ref_genome,
    '-miaf=f{2}'        => \@miaf,
    '-maaf=f{2}'        => \@maaf,
    '-ngap=f'           => \$ngap,
    '-filter|f=i'       => \$filter,
    '-gatk-bin=s'       => \$gatk,
    '-interval-dir|i=s' => \$interval_dir,
    '-threads|t=i'      => \$threads
) or die 'wrong input argument value(s).';

# Check arguements
if(!$input) {
	die 'You must provide an input vcf (-i).';
}
if(!-f$input) {
	die 'The provided input file does not exists.';
}
if(!$outbase) {
	die 'You must provide an output prefix base.';
}
if( !$ref_genome ) {
    die 'You must provide the reference genome used in GATK.';
}
if( !-f$ref_genome ) {
    die 'The provided reference genome file does not exist.';
}
if( $ngap ) {
    $filters{'n_gap'} = $ngap;
}
if( scalar(@miaf) == 2 ) {
    $filters{'min_af'} = $miaf[0];
    $filters{'n_min_af'} = $miaf[1];
}
if( scalar(@maaf) == 2 ) {
    $filters{'max_af'} = $maaf[0];
    $filters{'n_max_af'} = $maaf[1];
}

# Check GATK path
$gatk = check_version($gatk);

# Check vmtouch tool
check_vmtouch();

# Check the reference genome
my $ref_dict = check_reference($ref_genome);

# Add files to cache
my @caching_threads;
cache_files([$ref_genome, $ref_dict, $input], \@caching_threads);

# While caching file, prepare intervals
create_intervals($interval_dir, $gatk, $ref_genome, $threads, undef);

# Wait for file caching thread
foreach (@caching_threads) {
    $_->join();
}

# Prepare shared variables
my @seq_id;
my @seq_len;
get_seq_lengths($input, \@seq_id, \@seq_len);
my @data :shared;
foreach (0..($threads-1)) {
    my %detail :shared;
    my @sta :shared;
    my @sid :shared;
    my @loc :shared;
    my @alf :shared;
    $detail{'status'} = \@sta;
    $detail{'seq_id'} = \@sid;
    $detail{'location'} = \@loc;
    $detail{'af'} = \@alf;
    $data[$_] = \%detail;
}

# Start parsing threads
my @parse_threads;
foreach my $int (0..($threads-1)) {
    my $parse_thread = threads->new(\&parse_af_interval,
        $input, $int, $interval_dir, \@data, \%filters, \%flags);
    push @parse_threads, $parse_thread;
}

# Meanwhile, retrieve smp IDs
my @smp_ids;
get_sample_ids($input, \@smp_ids);

# Wait for parsing threads
foreach (@parse_threads) {
    $_->join();
}

# Write outputs
print_msg("Main thread", 1, "Writing data out...");
open(OUT1, '>'.$outbase.'.af') or die 'failed to create output file.';
open(OUT2, '>'.$outbase.'.filter') or die 'failed to create output file.';
open(OUT3, '>'.$outbase.'.kept') or die 'failed to create output file.';

# Print the headers
print OUT1 "CHROM\tPOS\t".join("\t", @smp_ids)."\n";

# Scan each SNP from each intervals
my $tot = 0;
my $kept = 0;
my $del = 0;
foreach my $int (0..($threads-1)) {
    foreach my $snp (0..(scalar(@{$data[$int]{status}})-1) ) {
        # Print filter data
        $tot++;
        print OUT2 $data[$int]{seq_id}[$snp]."\t";
        print OUT2 $data[$int]{location}[$snp]."\t";
        print OUT2 $data[$int]{status}[$snp]."\n";

        # Apply the filter and keep if necessary
        if( ($data[$int]{status}[$snp] & $filter) == 0) {
            # Print kept data
            $kept++;
            print OUT3 $data[$int]{seq_id}[$snp]."\t";
            print OUT3 $data[$int]{location}[$snp]."\t";
            print OUT3 $data[$int]{status}[$snp]."\n";

            # Print allele frequency data
            print OUT1 $data[$int]{seq_id}[$snp]."\t";
            print OUT1 $data[$int]{location}[$snp]."\t";
            print OUT1 $data[$int]{af}[$snp]."\n";
        } else {
            $del++;
        }
    }
}
close(OUT1);
close(OUT2);
close(OUT3);
print_msg("Main thread", 1, "Scanning $tot SNPs, keeping $kept, deleting $del.");