#!/usr/bin/env perl

use strict;
use warnings;
use Getopt::Long;

my $vcf_file;
my $output;

GetOptions(
    'v=s' => \$vcf_file,
    'o=s' => \$output
) or die 'Wrong argument value(s).';

if( !$vcf_file ) {
    die 'You must provide an input vcf file.';
} elsif( !-f$vcf_file ) {
    die 'The provided vcf file does not exist.';
}

open(VCF, $vcf_file) or die 'Failed to read the VCf file.';
open(OUT, '>'.$output) or die 'Failed to create output file.';

my $line = <VCF>;

# Copy the header
while($line =~ /^#/) {
    print OUT $line;
    $line = <VCF>;
}

# Stats
my $nsub = 0;
my $nins = 0;
my $ndel = 0;

# Variant loc
my %var;
my $last_chr = "";
my $last_pos = 0;
my $last_alt = "";

# Treat variants
while($line) {
    my @elems = split /\t/, $line;
    my $ref_len = length($elems[3]);
    my $alt_len = length($elems[4]);
    #if($elems[4] =~ /\*/ ) {
    
    # Replace * by -
    $elems[4] =~ s/\*/\-/g;

    # 1) Looking for the largest ALT
    my @ALT = split /,/, $elems[4];
    $alt_len = 0;
    foreach (@ALT) {
        if(length($_) > $alt_len) {
            $alt_len = length($_);
        }
    }

    # 2) Preparing the REF
    my @REF = split //, $elems[3];
    my @POS;
    foreach (0..($ref_len-1)) {
        push @POS, $elems[1]+$_;
    }

    # 3) Complete if ALT is larger
    my $to_add = 0;
    if( $ref_len < $alt_len ) {
        $to_add = $alt_len - $ref_len;
        $REF[$ref_len-1] .= '-'x$to_add;
    }

    # 4) Treat each alt
    my @ALT_EDIT = ("")x$ref_len;
    my $nadd = 0;
    foreach my $alt (@ALT) {
        my @a = split //, $alt;
        my $n = scalar(@a);

        # 4.1) alt shorter than ref
        if( $n < $ref_len ) {
            while(scalar(@a) < $ref_len) {
                push @a, '-';
            }
            if($to_add > 0) {
                $a[$ref_len-1] .= '-'x$to_add;
            }
            foreach (0..($ref_len-1)) {
                $ALT_EDIT[$_] .= $a[$_];
            }
        # 4.2) alt and ref same length
        } elsif( $n == $ref_len ) {
            if($to_add > 0) {
                $a[$ref_len-1] .= '-'x$to_add;
            }
            foreach (0..($ref_len-1)) {
                $ALT_EDIT[$_] .= $a[$_];
            }
        # 4.3) alt is larger
        } else {
            foreach (0..($ref_len-1)) {
                $ALT_EDIT[$_] .= $a[$_];
            }
            foreach (($ref_len..($n-1))) {
                $ALT_EDIT[$ref_len-1] .= $a[$_];
            }
        }
        $nadd++;

        # Add coma separator
        if( $nadd < scalar(@ALT) ) {
            foreach (0..($ref_len-1)) {
                $ALT_EDIT[$_] .= ',';
            }
        }
    }

    # Write every lines
    # Skip too complexe indels (dubious regions)
    if( $elems[0] eq $last_chr && $last_pos > $POS[0]) {
        print "WARNING: skip to complex INDEL: $line";
    } else {
        # Check particular case: double indels generated by GATK
        if($elems[0] eq $last_chr && $last_pos == $POS[0]) {
            # The nucleotide that is used to anchor the insertion should
            # not be conserved in the alternative form.
            print "WARNING: Rebase last alternative base ($last_alt).\n";
            my @tmp = split /,/, $ALT_EDIT[0];
            foreach (0..(scalar(@tmp)-1)){
                $tmp[$_] =~ s/^[ACGTacgt]/\-/;
            }
            $ALT_EDIT[0] = join(",", @tmp);
        }
        foreach (0..($ref_len-1)) {
            # Output only line with distinct REF and ALT
            if( $REF[$_] ne $ALT_EDIT[$_] ) {
                $elems[1] = $POS[$_];
                $elems[3] = $REF[$_];
                $elems[4] = $ALT_EDIT[$_];
                my $edited = join("\t", @elems);
                print OUT $edited;

                my $vid = $elems[0].'_'.$elems[1];
                if( exists($var{$vid}) ) {
                    print "WARNING: location involved at least twice by variants: $elems[0] at $elems[1].\n";
                } else {
                    $var{$vid} = 1;
                }
                $last_chr = $elems[0];
                $last_pos = $POS[$_];
                $last_alt = $ALT_EDIT[$_];
            }
        }
    }
    $line = <VCF>;
}
close(VCF);
close(OUT);

#my $tot = $nsub + $nins + $ndel;
#print "Treated $tot variants:\n";
#print "  - $nsub substitutions (SNP),\n";
#print "  - $nins insertions,\n";
#print "  - $ndel deletions.\n";