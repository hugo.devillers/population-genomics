#!/usr/bin/env perl

use strict;
use warnings;
use Getopt::Long;

my $input;
my $output;
my $n_snps = 1000;
GetOptions(
	'i=s' => \$input,
	'o=s' => \$output,
    'n=i' => \$n_snps
) or die 'Wrong argument values.';

if(!$input) {
	die 'You must provide an input vcf (-i).';
}
if(!-f$input) {
	die 'The provided input file does not exists.';
}
if(!$output) {
	die 'You must provide an output vcf path (-o).';
}

open(VCF, $input) or die 'Failed to open the input file.';
open(OUT, '>'.$output) or die 'Failed to create the output file.';

# Reach the header line
my $line = <VCF>;
while($line && $line !~ /^#CHROM/ ) {
	$line = <VCF>;
}
if(!$line) {
	die 'Failed to find the header line';
}

# Get all the SNP Ids
my @ids;
while($line = <VCF>) {
    my @elem = split /\t/, $line;
    push @ids, $elem[2];
}
close(VCF);

# Get the selection of SNP
if(scalar(@ids) < $n_snps) {
    die 'The number of required SNPs is higher than the number of SNPs in the VCF file.';
}
my $n_left = $n_snps;
my $n_ids = scalar(@ids);
my @kept;
while($n_left > 0) {
    my $i = int(rand($n_ids));
    push @kept,$ids[$i];
    $ids[$i] = $ids[$n_ids-1];
    $n_left--;
    $n_ids--;
}

# Reorder them
@kept = sort {$a cmp $b} @kept;

# Print out
foreach (@kept) {
    print OUT $_."\n";
}
close(OUT);
