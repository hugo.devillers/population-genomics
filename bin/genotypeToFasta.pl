#!/usr/bin/env perl
use strict;
use warnings;
use Getopt::Long;
use Bio::SeqIO;
use Bio::Seq;

my $input;
my $output;
my $no_gap;
GetOptions(
	'i=s' => \$input,
	'o=s' => \$output,
	'ng'  => \$no_gap
) or die 'wrong input argument value.';

if(!$input) {
	die 'You must provide an input vcf (-i).';
}
if(!-f$input) {
	die 'The provided input file does not exists.';
}
if(!$output) {
	die 'You must provide an output vcf path (-o).';
}

open(TXT, $input) or die 'Failed to open input file.';

# Skip header lines
my $line = <TXT>;
while($line =~ /^##/) {
	$line = <TXT>;
}

# Extract header
my %seq;
my @ids;
chomp($line);
my @head = split(/\t/, $line);
my $last_smp = scalar(@head) - 1;
foreach (4..$last_smp) {
	my $id = $head[$_];
	$id =~ s/\.GT//;
	$seq{$id} = '';
	push @ids, $id;
}

while($line = <TXT>) {
	# Skip line if there gaps while ng is on
	if($no_gap && $line =~ /\.[\|\/]\./) {
		next;
	}
	chomp($line);
	# Remove genotype separator
	$line =~ s/[\|\/]//g;

	# Transform gaps
	$line =~ s/\.\./\-\-/;

	# Parse samples
	my @smp = split /\t/, $line;
	foreach (4..$last_smp) {
		$seq{$ids[$_ - 4]} .= $smp[$_];
	}
}
close(TXT);

# Save sequences
my $seq_out = Bio::SeqIO->new(-file => '>'.$output, -format => 'fasta');
foreach (@ids) {
	my $s = Bio::Seq->new(
		-id => $_,
		-seq => $seq{$_}
	);
	$seq_out->write_seq($s);
}



	
