#!/usr/bin/env perl

use strict;
use warnings;
use Getopt::Long;
use POSIX qw(floor);
use PopGeno::vcfutils qw(get_format_index get_sample_data get_sample_number);

# ARGUMENTS
my $input;
my $output;
my $remove;
my $n_gt = -1;
my $f_gt = -1;
GetOptions(
	'-input|i=s'       => \$input,
	'-output-base|o=s' => \$output,
    '-n=i'             => \$n_gt,
    '-f=f'             => \$f_gt,
    '-remove|r'        =>  \$remove
) or die 'Wrong input argument value.';

# ARGUMENT CHECK
if(!$input) {
	die 'You must provide an input vcf (-i).';
}
if(!-f$input) {
	die 'The provided input file does not exists.';
}
if(!$output) {
	die 'You must provide an output base path (-o).';
}
if( $n_gt != -1 && $f_gt != -1 ) {
    die 'You cannot define both -f and -n.';
}

# OPEN/CREATE FILES
open(VCF, $input) or die 'Failed to open the input file.';
open(KEP, '>'.$output.'.kept') or die 'Failed to create output kept list.';
open(DIS, '>'.$output.'.disc') or die 'Failed to create output discard list.';
my $OUT;
if( $remove ) {
    open($OUT, '>'.$output.'.vcf') or die 'Failed to create output vcf file.';
}

# TREAT THE HEADER
my @header_lines;
my $line = <VCF>;
while($line && $line !~ /^#CHROM/ ) {
	push @header_lines, $line;
	$line = <VCF>;
}
if(!$line) {
    die 'Possible bad VCF format.';
}
if( $remove ) {
    foreach (@header_lines) {
        print $OUT $_;
    }
    print $OUT $line;
}

# TREAT VARIANT LINES
$line = <VCF>;
my $fi = get_format_index($line, 'GT');
my $sn = get_sample_number($line);
if($f_gt != -1) {
    $n_gt = floor($sn * $f_gt)
}
my $n_uniq = 0;
my $n_lowf = 0;
my $n_kept = 0;
my $n_tot = 0;
LINES:while($line) {
    # Remove phasing information
    my $no_phase = $line;
    $no_phase =~ s/\|/\//g;
    $n_tot++;

    # Get genotypes
    my @genotypes;
    my %snp_info;
    get_sample_data($no_phase, \@genotypes, $fi, \%snp_info);

    # Count different genotypes
    my %cnt;
    foreach (@genotypes) {
        $cnt{$_} += 1;
    } 

    if(scalar(keys %cnt) == 1) {
        # Only one genotype => discard
        print DIS $snp_info{chr}."\t".$snp_info{pos}."\n";
        $n_uniq++;
    } else {
        if($n_gt != -1) {
            # We need at least two genotypes with more than n_gt 
            # representative samples
            my $n_more = 0;
            foreach (keys %cnt) {
                if($cnt{$_} > $n_gt) {
                    $n_more++;
                }
            }
            if($n_more <= 1) {
                # Not enough => discard
                print DIS $snp_info{chr}."\t".$snp_info{pos}."\n";
                $n_lowf++;
                $line = <VCF>;
                next LINES;
            }
        }

        # Keep it
        print KEP $snp_info{chr}."\t".$snp_info{pos}."\n";
        $n_kept++;
        if( $remove ) {
            print $OUT $line;
        } 
    }
    $line = <VCF>;
}
close(KEP);
close(DIS);
close(VCF);
if($remove) {
    close($OUT);
}


print "Among $n_tot variants:\n";
print " - $n_kept were kept,\n";
print " - $n_uniq were discarded (only one genotype)";
if($n_gt != -1) {
    print ",\n - $n_lowf were discarded (too low frequency).\n";
}
else {
    print ".\n";
}