#!/usr/bin/env perl

use strict;
use warnings;
use Getopt::Long;
use threads;
use threads::shared;
use PopGeno::message qw(print_msg);
use PopGeno::caching qw(check_vmtouch cache_files);
use PopGeno::reference qw(check_reference);
use PopGeno::gatk qw(check_version gather genotypegvcfs);
use PopGeno::intervals qw(create_intervals_by_chr);

# Init. parameter values
my $gatk;
my $threads = 4;
my $interval_dir = 'interval_lists';
my $ref_genome;
my $gvcf;
my $base_out;

# Retrieve parameter values
GetOptions(
    '-ref-genome|r=s'   => \$ref_genome,
    '-output-base|o=s'  => \$base_out,
    '-gatk-bin|g=s'     => \$gatk,
    '-interval-dir|i=s' => \$interval_dir,
    '-gvcf|v=s'         => \$gvcf,
    '-threads|t=i'      => \$threads
) or die 'Wrong input parameter value(s).';


# Check arguments
if( !$gvcf ) {
    die 'You must provide a gvcf file.';
}
if( !-f$gvcf ) {
    die 'The provided gvcf file does not exists.';
}

# Check GATK path
$gatk = check_version($gatk);

# Check vmtouch tool
check_vmtouch();

# Check the reference genome
my $ref_dict = check_reference($ref_genome);

# Caching files
my @files_to_cache;
push @files_to_cache, $ref_genome;
push @files_to_cache, $ref_genome.'.fai';
push @files_to_cache, $ref_dict;
push @files_to_cache, $gvcf;
my @caching_threads;
cache_files(\@files_to_cache, \@caching_threads);

# While caching file, prepare intervals
create_intervals_by_chr($interval_dir, $gatk, $ref_genome, $threads);

# Wait for file caching thread
foreach (@caching_threads) {
    $_->join();
}

# Genotype GVCF
my @gatk_threads;
my @sub_vcfs :shared;
my @logs :shared;
foreach my $int (0..($threads-1)){
    my $gatk_thread = threads->new(\&genotypegvcfs, $gatk, $interval_dir, 
        $int, $gvcf, $ref_genome, $base_out, \@sub_vcfs, \@logs);
    push @gatk_threads, $gatk_thread;
}
foreach (@gatk_threads) { $_->join(); }

# Gather splitted vcf files
gather($gatk, \@sub_vcfs, $base_out, 0);

# Cleaning
print_msg('Main thread', 1, 'Cleaning up temporary files...');
foreach (@sub_vcfs) { 
    unlink $_;
    unlink $_.'.idx';
}
foreach (@logs) {
    unlink $_;
}
print_msg('Main thread', 1, 'All processes completed.');