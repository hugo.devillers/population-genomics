#!/usr/bin/env perl

use strict;
use warnings;
use Getopt::Long;
use PopGeno::vcfutils qw(get_contig_length);

# ARGUMENTS
my $input; # variants
my $region_file;
my $region_fofn;
my $output;
my $min_cover = 1;
my $reverse;
my $skip_header;
GetOptions(
    '-input|i=s'       => \$input,
    '-region-file|r=s' => \$region_file,
    '-region-fofn|f=s' => \$region_fofn,
    '-output|o=s'      => \$output,
    '-skip-header|s'   => \$skip_header,
    '-min-cover|m=i'   => \$min_cover,
    '-reverse'         => \$reverse
) or die 'Wrong input argument value(s).';

# CHECK ARGUMENTS
if( !$input ) {
    die 'You must provide an input VCF file.';
}
if( !-f$input ) {
    die 'The input VCF file does not exists.';
}
if( $region_file && $region_fofn ) {
    die 'You cannot provide both a single file and a file of file names for regions.';
}
if( $region_file ) {
    if( !-f$region_file ) {
        die 'The region file does not exists.';
    }
}
if( $region_fofn ) {
    if( !-f$region_fofn ) {
        die 'The region file of file names does not exists.';
    }
}

# SUB DEFINITION
sub add_coverage {
    my $array = shift;
    my $from  = shift;
    my $to    = shift;

    foreach ($from..$to) {
        $array->[$_] += 1;
    }
}

# READ THE VCF HEADER AND GET CHROMOSOME LENGTHES
open(VCF, $input) or die 'Failed to open the input file.';
open(OUT, '>'.$output) or die 'Failed to create the output file.';
# Reach the contig description lines
my $line = <VCF>;
while($line && $line !~ /^##contig/) {
	print OUT $line;
	$line = <VCF>;
}
if(!$line) {
	die 'Possible bad VCF format.';
}
# Get chromosome lengthes
my %chr_len;
while($line && $line =~ /^##contig/ ) {
    get_contig_length($line, \%chr_len);
    print OUT $line;
    $line = <VCF>;
}
if( !$line ) {
    die 'Possible bad VCF format.';
}
# Finish to read the header
while($line && $line !~ /^#CHROM/ ) {
	print OUT $line;
	$line = <VCF>;
}
if(!$line) {
	die 'Failed to find the header line';
}
print OUT $line;
$line = <VCF>;

# PREPARE REGION DATA
my %chr_cov;
foreach (keys %chr_len) {
    @{$chr_cov{$_}} = (0)x$chr_len{$_};
}
# Prepare the list of region file(s)
my @rfiles;
if( $region_file ) {
    push @rfiles, $region_file;
} elsif ( $region_fofn ) {
    open(FOF, $region_fofn) or die 'Failed to open file of filename file.';
    while(my $f = <FOF>) {
        chomp($f);
        if( !-f$f ) {
            die "The file $f does not exist.";
        }
        push @rfiles, $f;
    }
}
# Compute region coverage
foreach my $rfile (@rfiles) {
    open(REG, $rfile) or die "Failed to open $rfile.";
    my $region = <REG>;

    if( $skip_header ) {
        $region = <REG>;
    }
    
    while($region) {
        chomp($region);
        my @elems = split /\t/, $region;
        if( !exists($chr_len{$elems[0]})) {
            die "Region missing from the VCF file ($elems[0]).";
        }

        add_coverage($chr_cov{$elems[0]}, $elems[1]-1, $elems[2]-1);

        $region = <REG>;
    }
    close(REG);
}

# SCAN VARIANTS
my $n_tot = 0;
my $n_kept = 0;
my $n_disc = 0;
# Treat in reverse mode
if( $reverse ) {
    while( $line ) {
        $n_tot++;
        my @elems = split /\t/, $line;
        if( $chr_cov{$elems[0]}[$elems[1]-1] < $min_cover) {
            $n_kept++;
            print OUT $line;
        }
        $line = <VCF>;
    }
} else {
    while( $line ) {
        $n_tot++;
        my @elems = split /\t/, $line;
        if( $chr_cov{$elems[0]}[$elems[1]-1] >= $min_cover) {
            $n_kept++;
            print OUT $line;
        }
        $line = <VCF>;
    }
}
close(OUT);
close(VCF);

$n_disc = $n_tot - $n_kept;
print "Kept $n_kept SNPs among a total of $n_tot SNPs.\n";
print "Discarded $n_disc SNPs.\n";