#!/usr/bin/env perl

use strict;
use warnings;
use Getopt::Long;
use PopGeno::vcftools qw(apply_genotypes);
use PopGeno::vcfutils qw(get_sample_ids);
use gryc::utilities::sequenceLoader qw(loadSequences);
use gryc::utilities::sequenceEditor qw(reverseComplement);
use Bio::SeqIO;
use Bio::Seq;

my $seq_file;
my $vcf_file;
my $gff_file;
my $smp_id;
my $gff_id = 'ID';
my $gff_target = 'CDS';
my $out_base;
GetOptions(
    's=s' => \$seq_file,
    'v=s' => \$vcf_file,
    'g=s' => \$gff_file,
    'i=s' => \$smp_id,
    'n=s' => \$gff_id,
    't=s' => \$gff_target,
    'o=s' => \$out_base
) or die 'Wrong argument value(s).';

if( !$seq_file ) {
    die 'You must provide an input fasta file.';
} elsif( !-f$seq_file ) {
    die 'The provided fasta file does not exist.';
}
if( !$vcf_file ) {
    die 'You must provide an input vcf file.';
} elsif( !-f$vcf_file ) {
    die 'The provided vcf file does not exist.';
}
if( !$smp_id ) {
    my @ids;
    get_sample_ids($vcf_file, \@ids);
    $smp_id = $ids[0];
    print 'No sample ID provided, considering the first sample ('.$smp_id.')'."\n";
}

# Load the sequences
my @seqs;
my $nseqs = loadSequences($seq_file, \@seqs, 'fasta');

# Apply genotypes to the genome
my @gt_seqs;
apply_genotypes(\@seqs, $vcf_file, $smp_id, \@gt_seqs);

print scalar(@gt_seqs)."\n";

# Prepare output (depending on gff data)
my $seq_out = Bio::SeqIO->new(
    -file => '>'.$out_base.".fasta",
    -format => 'fasta'
);
if( !$gff_file ) {
    # No gff file, treat the complete chromomes
    foreach my $s (keys %{$gt_seqs[0]}) {
        foreach my $p (0..(scalar(@gt_seqs)-1)) {
            my $new_seq = Bio::Seq->new(
                -id => $s.'_GT'.$p,
                -seq => $gt_seqs[$p]{$s}
            );
            $seq_out->write_seq($new_seq);
        }
    }
} else {
    # Parse GFF file
    open(GFF, $gff_file);
    my $line = <GFF>;
    while($line =~ /^#/ ) {
        $line = <GFF>;
    }
    my $current_id = "";
    my @current_seqs = ();
    my $current_strand = "+";
    while($line) {
        chomp($line);
        my @elems = split /\t/, $line;
        if( $elems[2] eq $gff_target ) {
            if( $elems[8] =~ /$gff_id=(\w+)/ ) {
                my $id = $1;
                if( $id ne $current_id ) {
                    # Save the sequence
                    if( $current_id ne '' ) {
                        if($current_strand eq '+') {
                            foreach my $p (0..(scalar(@gt_seqs)-1)) {
                                my $new_seq = Bio::Seq->new(
                                    -id => $current_id.'_GT'.$p,
                                    -seq => $current_seqs[$p]
                                );
                                $seq_out->write_seq($new_seq);
                                $current_seqs[$p] = "";
                            }
                        } else {
                            foreach my $p (0..(scalar(@gt_seqs)-1)) {
                                my $dna = reverseComplement($current_seqs[$p]);
                                my $new_seq = Bio::Seq->new(
                                    -id => $current_id.'_GT'.$p,
                                    -seq => $dna
                                );
                                $seq_out->write_seq($new_seq);
                                $current_seqs[$p] = "";
                            }
                        }
                    }
                    $current_id = $id;
                    $current_strand = $elems[6];
                }

                foreach my $p (0..(scalar(@gt_seqs)-1)) {
                    my $len = $elems[4] - $elems[3] + 1;
                    $current_seqs[$p] .= substr($gt_seqs[$p]{$elems[0]}, $elems[3]-1, $len);
                }
            } else {
                die 'Failed to find the feature ID in line: '.$line."\n";
            }
        }
        $line = <GFF>;
    }
    # Save the last sequence
    if($current_strand eq '+') {
        foreach my $p (0..(scalar(@gt_seqs)-1)) {
            my $new_seq = Bio::Seq->new(
                -id => $current_id.'_GT'.$p,
                -seq => $current_seqs[$p]
            );
            $seq_out->write_seq($new_seq);
        }
    } else {
        foreach my $p (0..(scalar(@gt_seqs)-1)) {
            my $dna = reverseComplement($current_seqs[$p]);
            my $new_seq = Bio::Seq->new(
                -id => $current_id.'_GT'.$p,
                -seq => $dna
            );
            $seq_out->write_seq($new_seq);
        }
    }
    close(GFF);
}