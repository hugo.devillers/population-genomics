#!/usr/bin/env perl

use strict;
use warnings;
use Getopt::Long;

my $input;
my $output;
my $snp_list;
my $discard;
GetOptions(
	'i=s' => \$input,
	'o=s' => \$output,
    'l=s' => \$snp_list,
    'd'   => \$discard
) or die 'wrong input argument value.';

if(!$input) {
	die 'You must provide an input vcf (-i).';
}
if(!-f$input) {
	die 'The provided input file does not exists.';
}
if(!$snp_list) {
    die 'You must provide an SNP list file.';
}
if(!-f$snp_list) {
    die 'The provided SNP list file does not exists.';
}
if(!$output) {
	die 'You must provide an output vcf path (-o).';
}

open(VCF, $input) or die 'Failed to open the input file.';
open(OUT, '>'.$output) or die 'Failed to create the output file.';

# First, retrieved the VCF contig list
my $line = <VCF>;

# Reach the contig description lines
while($line && $line !~ /^##contig/) {
	print OUT $line;
	$line = <VCF>;
}
if(!$line) {
	die 'Possible bad VCF format.';
}

# Build a name converter hash
my @chr_id;
while($line =~ /^##contig/ ) {
	if($line =~ /ID=([\w\.]+)\,/ ) {
        push @chr_id, $1;
		print OUT $line;
	} else {
		die 'failed to identify chromosome names.';
	}
	$line = <VCF>;
}
# Reach the header line
while($line && $line !~ /^#CHROM/ ) {
	print OUT $line;
	$line = <VCF>;
}
if(!$line) {
	die 'Failed to find the header line';
}
print OUT $line;

# Read and parse the list file
open(TXT, $snp_list) or die 'Failed to open de SNP list file.';
my %snps;
my $snp = <TXT>;
my $n_exp = 0;
if( $snp =~ /^\d+\t/ ) {
    while( $snp ) {
        $n_exp++;
        chomp($snp);
        my @tmp = split(/\t/, $snp);
        my $id = $chr_id[$tmp[0]-1].'_'.$tmp[1];
        $snps{$id} = 1;
        $snp = <TXT>;
    }
} else {
    while( $snp ) {
        $n_exp++;
        chomp($snp);
        my @tmp = split(/\t/, $snp);
        my $id = $tmp[0].'_'.$tmp[1];
        $snps{$id} = 1;
        $snp = <TXT>;
    }
}
close(TXT);

# Scan all SNP a keep only those from the list
my $n_kept = 0;
my $n_disc = 0;
if($discard) {
    while($line = <VCF>) {
        my @elems = split(/\t/, $line, 3);
        my $id = $elems[0].'_'.$elems[1];
        if( exists($snps{$id})) {
            $n_disc++;
        } else {
            $n_kept++;
            print OUT $line;
        }
    }
} else {
    while($line = <VCF>) {
        my @elems = split(/\t/, $line, 3);
        my $id = $elems[0].'_'.$elems[1];
        if( exists($snps{$id})) {
            $n_kept++;
            print OUT $line;
        } else {
            $n_disc++;
        }
    }
}
close(VCF);
close(OUT);

print "Kept $n_kept SNPs ($n_exp SNPs listed in the provided file).\n";
print "Discarded $n_disc SNPs.\n";
