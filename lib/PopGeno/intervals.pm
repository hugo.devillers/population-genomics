package PopGeno::intervals;

require Exporter;
use strict;
use warnings;
use PopGeno::message qw(print_msg);

our @ISA    = qw(Exporter);
our @EXPORT = qw(create_intervals create_intervals_by_chr create_intervals_no_chr_cut);

sub create_intervals {
    my $interval_dir = shift; # Interval directory
    my $gatk         = shift; # GATK binary
    my $ref_genome   = shift; # Reference genome path
    my $n_scatters   = shift; # Number of scatter (intervals)
    my $input_bam    = shift; # Input BAM file

    # Check interval dir
    if( !$interval_dir ) {
        die 'You must provide an interval directory.';
    }
    if( !-d $interval_dir ) {
        mkdir $interval_dir or die 'Failed to create interval directory';
    }

    # Create intervals with GATK
    print_msg('Main thread', 1, 'Preparing intervals...');
    my $log_file = $interval_dir.'/SplitInterval.log';
    if( defined($input_bam) ) {
        my $cmd = `$gatk SplitIntervals -R $ref_genome -I $input_bam -scatter $n_scatters -O $interval_dir 1>$log_file 2>&1`;
    } else {
        my $cmd = `$gatk SplitIntervals -R $ref_genome -scatter $n_scatters -O $interval_dir 1>$log_file 2>&1`;
    }
    print_msg('Main thread', 1, 'Intervals done.');
}

sub create_intervals_by_chr {
    my $interval_dir = shift; # Interval directory
    my $gatk         = shift; # GATK binary
    my $ref_genome   = shift; # Reference genome path
    my $n_scatters   = shift; # Number of scatter (intervals)

    # Check interval dir
    if( !$interval_dir ) {
        die 'You must provide an interval directory.';
    }
    if( !-d $interval_dir ) {
        mkdir $interval_dir or die 'Failed to create interval directory';
    }

    # Create intervals with GATK
    print_msg('Main thread', 1, 'Preparing intervals...');
    my $log_file = $interval_dir.'/SplitInterval.log';
    my $cmd = `$gatk SplitIntervals -R $ref_genome -scatter $n_scatters -mode INTERVAL_COUNT -O $interval_dir 1>$log_file 2>&1`;
    print_msg('Main thread', 1, 'Intervals done.');
}

sub create_intervals_no_chr_cut {
	my $interval_dir = shift; # Interval directory
	my $gatk         = shift; # GATK binary
	my $ref_genome   = shift; # Reference genome path
	my $n_scatters   = shift; # Number of scatter (intervals)

	# Check interval dir
	if( !$interval_dir ) {
		die 'You must provide an interval directory.';
	}
	if( !-d $interval_dir ) {
		mkdir $interval_dir or die 'Failed to create interval directory';
	}

	# Create intervals with GATK
	print_msg('Main thread', 1, 'Preparing intervals...');
	my $log_file = $interval_dir.'/SplitInterval.log';
	my $cmd = `$gatk SplitIntervals -R $ref_genome -scatter $n_scatters -mode BALANCING_WITHOUT_INTERVAL_SUBDIVISION -O $interval_dir 1>$log_file 2>&1`;
	print_msg('Main thread', 1, 'Intervals done.');
}
