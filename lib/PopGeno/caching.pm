package PopGeno::caching;

require Exporter;
use strict;
use warnings;
use PopGeno::message qw(print_msg);

our @ISA    = qw(Exporter);
our @EXPORT = qw(check_vmtouch check_cached cache_file cache_files);

# Check vmtouch binary
sub check_vmtouch {
    my $vmtouch = `which vmtouch`;
    if( !$vmtouch ) {
        die 'You must install vmtouch tool (APT or Git clone).';
    }
}

# Check if a file is cached
sub check_cached {
    my $file = shift;
    my $test = `vmtouch $file`;
    $test =~ /(\d+)\%/;
    return $1;
}

# Add a given file into cache
sub cache_file {
    my $file = shift;
    my $ith  = shift;
    my $thn  = "Caching thread";

    print_msg($thn,$ith, "Check if the file $file is already cached...");
    my $check = check_cached($file);
    if( $check == 100 ) {
        print_msg($thn,$ith, "The file $file is already cached.");
    } else {
        print_msg($thn,$ith, "The file $file is not completely cached ($check\%).");
        print_msg($thn,$ith, "Adding the file $file into the cache...");
        my $add = `vmtouch -t $file`;
        $check = check_cached($file);
        if( $check == 100 ) {
            print_msg($thn,$ith, "The file $file is now cached.");
        }
    }
}

# Add a list of file using multiple threads
sub cache_files {
    my $files_to_cache = shift; # Array ref containing file pathes
    my $caching_threads = shift; # Array ref (empty)

    my $id_thread = 1;
    foreach (@{$files_to_cache}) {
        my $caching_thread = threads->new(\&cache_file, $_, sprintf("%03d", $id_thread));
        push @{$caching_threads}, $caching_thread;
        $id_thread++;
    }
}

return 1;