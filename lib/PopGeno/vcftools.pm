package PopGeno::vcftools;

require Exporter;
use strict;
use warnings;

our @ISA    = qw(Exporter);
our @EXPORT = qw(apply_genotypes);

sub apply_genotypes {
    my $seq_data  = shift; # Array ref of Bio::Seq
    my $vcf_file  = shift; # Path to vcf file
    my $sample_id = shift; # Select a sample ID
    my $seq_out   = shift; # Hash ref Edited sequence data (output)

    # Open the VCF file and parse it
    open(VCF, $vcf_file) or die 'Failed to open the input VCF file.';
    # Skip header
    my $line = <VCF>;
    while( $line =~ /^##/ ) {
        $line = <VCF>;
    }
    # Find sample column
    my $smp = 0;
    if( $line =~ /^#CHROM/ ) {
        chomp($line);
        my @elems = split /\t/, $line;
        SMP: foreach (9..(scalar(@elems)-1)) {
            if( $elems[$_] eq $sample_id ) {
                $smp = $_;
                last SMP;
            }
        }
        if( $smp == 0 ) {
            die 'Failed to find the sample ID in the VCF file.';
        }
    } else {
        die 'No header found in VCF file.';
    }
    # Get the ploidy and GT index
    $line = <VCF>;
    my $ploidy = 0;
    my $gti = 0;
    if( $line ) {
        my @elems = split /\t/, $line;
        my @info = split /:/, $elems[9];
        my @AD = split /,/, $info[1];
        $ploidy = scalar(@AD);

        my @format = split /:/, $elems[8];
        FORMAT: foreach (0..(scalar(@format)-1)) {
            if( $format[$_] eq 'GT' ) {
                $gti = $_;
                last FORMAT;
            }
        }
    } else {
        die 'Empty VCF file.';
    }
    if( $ploidy == 0 ) {
        die 'Failed to identify ploidy from the VCF file';
    }

    # Convert the sequence data into array of strings
    my @seq_edit;
    foreach my $p (0..($ploidy-1)) {
        $seq_edit[$p] = {};
    }
    foreach my $seq (@{$seq_data}) {
        my $s = $seq->display_id();
        my @seq_spl = split //, $seq->seq();
        foreach my $p (0..($ploidy-1)) {
            @{$seq_edit[$p]{$s}} = @seq_spl;
        }
    }

    # Scan every variant lines
    while ($line) {
        my @elems = split /\t/, $line;
        my @info = split /:/, $elems[$smp];
        my @GT = split /[\/\|]/, $info[$gti];
        my @RA;
        push @RA, $elems[3];
        my @ALT = split ',', $elems[4];
        foreach (@ALT) {
            push @RA, $_;
        }

        # Check sequence correspondance
        if(!exists($seq_edit[0]{$elems[0]})) {
            die 'The sequence ID '.$elems[0].' from the VCF is not found in the sequence data.';
        }

        # Edit sequences
        foreach (0..($ploidy-1)) {
            $seq_edit[$_]{$elems[0]}[$elems[1]-1] = $RA[$GT[$_]];
        }

        # Next line
        $line = <VCF>;
    }
    close(VCF);

    # Fill the output variable
    foreach my $p (0..($ploidy-1)) {
        my %dt;
        foreach my $s (keys %{$seq_edit[$p]}) {
            my $dna = join '', @{$seq_edit[$p]{$s}};
            $dt{$s} = $dna;
        }
        push @{$seq_out}, \%dt;
    }
}