package PopGeno::message;

require Exporter;
use strict;
use warnings;

our @ISA    = qw(Exporter);
our @EXPORT = qw(print_msg);

# Print a formated message
sub print_msg {
    my $thn = shift;
    my $thi = shift;
    my $msg = shift;
    my $ltm = localtime();
    print "[$thn #$thi]($ltm): $msg\n";
}

return 1;