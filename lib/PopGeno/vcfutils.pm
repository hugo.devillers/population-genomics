package PopGeno::vcfutils;

require Exporter;
use strict;
use warnings;

our @ISA    = qw(Exporter);
our @EXPORT = qw(get_seq_lengths get_sample_ids get_format_index get_sample_data get_sample_number get_contig_length);

# Scan a ##contig line from VCF header
sub get_contig_length {
    my $line = shift;
    my $data = shift;

    if($line =~ /ID=([\w\.]+)\,/) {
        my $id = $1;
        if( $line =~ /length=(\d+)/ ) {
            $data->{$id} = $1;
        } else {
            die 'Failed to identify chromosome length ('.$line.').';
        }
	} else {
	    die 'failed to identify chromosome names ('.$line.').';
	}
}

# Read a GATK vcf file and complete two array refs
# with sequence IDs and lengths
sub get_seq_lengths {
    my $vcf     = shift;
    my $seq_id  = shift;
    my $seq_len = shift;

    # Open the VCF file
    open(VCF, $vcf) or die 'Failed to open the provided VCF file.';
    my $line = <VCF>;

    # Reach the contig description lines
    while($line && $line !~ /^##contig/) {
	    $line = <VCF>;
    }
    if(!$line) {
	    die 'Reach the end of the file without finding contig set. Possible bad VCF format.';
    }

    # Build a name converter hash
    my $chri = 0;
    while($line =~ /^##contig/ ) {
	    if($line =~ /ID=([\w\.]+)\,/) {
		    $chri++;
            my $id = $1;
            if( $line =~ /length=(\d+)/ ) {
                push @{$seq_id}, $id;
                push @{$seq_len}, $1;
            } else {
                die 'Failed to identify chromosome length ('.$line.').';
            }
	    } else {
		    die 'failed to identify chromosome names ('.$line.').';
	    }
	    $line = <VCF>;
    }
    if($chri == 0) {
	    die 'Failed to identify chromosome names and lengths.';
    }
    close(VCF);
}

# Read a GATK vcf file and retrieve sample IDs
sub get_sample_ids {
    my $vcf     = shift;
    my $smp_ids = shift;

    # Open the VCF file
    open(VCF, $vcf) or die 'Failed to open the provided VCF file.';
    my $line = <VCF>;

    # Reach the contig description lines
    while($line && $line !~ /^#CHROM/) {
	    $line = <VCF>;
    }
    if(!$line) {
	    die 'Reach the end of the file without finding header line. Possible bad VCF format.';
    }
    close(VCF);

    # Split header and get sample IDs
    chomp($line);
    my @head = split /\t/, $line;
    foreach ( 9..(scalar(@head)-1) ) {
        push @{$smp_ids}, $head[$_];
    }
}

# Get data index in format entry
sub get_format_index {
    my $line = shift;
    my $what = shift;

    my @elems = split /\t/, $line;
    my @format = split /:/, $elems[8];
    
    my $index = 0;
    while($index < scalar(@format)) {
        if($format[$index] eq $what ) {
            return $index;
        }
        $index++;
    }

    die "Entry type $what does not exist in format definition.";
}

# Count the number of sample
sub get_sample_number {
    my $line = shift;

    my @elems = split /\t/, $line;

    return(scalar(@elems)-9);
}

# Get sample data from a given line
sub get_sample_data {
    my $line = shift;
    my $data = shift;
    my $index = shift;
    my $snp_info = shift;

    my @elems = split /\t/, $line;

    foreach my $i (9..(scalar(@elems)-1)) {
        my @sample = split /:/, $elems[$i];
        push @{$data}, $sample[$index];
    }

    if(defined($snp_info)) {
        $snp_info->{chr} = $elems[0];
        $snp_info->{pos} = $elems[1];
        $snp_info->{nam} = $elems[2];
    }
}


return 1;