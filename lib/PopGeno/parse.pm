package PopGeno::parse;

require Exporter;
use strict;
use warnings;
use PopGeno::message qw(print_msg);

our @ISA    = qw(Exporter);
our @EXPORT = qw(parse_af_interval);

# Function that treat a given interval
sub parse_af_interval {
    my $input   = shift;
    my $int_id  = shift;
    my $int_dir = shift;
    my $data    = shift;
    my $filters = shift;
    my $flags   = shift;

    print_msg("Parsing thread", $int_id, "Start parsing interval $int_id...");

    # 1) Check the interval and read it
    my $int_file = $int_dir.'/'.sprintf("%04d", $int_id)."-scattered.interval_list";
    if( !-f$int_file ) {
        die 'The interval file ('.$int_file.') does not exist.';
    }
    open(INT, $int_file) or die 'Failed to open the interval file ('.$int_file.').';
    my $int_line = <INT>;
    my %int_data;
    while($int_line =~ /^\@/ ) {
        $int_line = <INT>;
    }
    while( $int_line ) {
        my @int = split /\t/, $int_line;
        %{$int_data{$int[0]}} = ('from' => $int[1], 'to' => $int[2]);
        $int_line = <INT>;
    }
    close(INT);

    # 2) Read input VCF file
    open(VCF, $input) or die 'Failed to open the input VCF file ('.$input.').';
    my $line = <VCF>;
    
    # 2.1) Skip the header
    while( $line && $line !~ /^#CHROM/ ) {
        $line = <VCF>;
    }
    $line = <VCF>;

    # 2.2) Identify the AD column
    my @tmp = split /\t/, $line;
    my @gts = split /\:/, $tmp[8]; # Genotype headers
    my $iad = 0;    
    foreach (@gts) {
        if( $_ eq 'AD') {
            last;
        }
        $iad++;
    }
    if( $iad == scalar(@gts) ) {
        die 'Failed to find the allele depth (AD) genotype header.';
    }

    # 2.3) Get the number of samples and init filter thresholds
    my $ns = scalar(@tmp) - 9;
    my $th_ngap = $filters->{n_gap};
    if( $th_ngap <= 1 ) {
        # This is a proportion
        $th_ngap = $th_ngap * $ns;
    }
    my $th_nmiaf = $filters->{n_min_af};
    if( $th_nmiaf < 1 ) {
        # This is a proportion
        $th_nmiaf = $th_nmiaf * $ns;
    }
    my $th_nmaaf = $filters->{n_max_af};
    if( $th_nmaaf <= 1 ) {
        # This is a proportion
        $th_nmaaf = $th_nmaaf * $ns;
    }


    # 2.4) Reach the required sequence ID
    my $seq_ids = '^('.join('|', keys %int_data).')';
    while( $line && $line !~ /$seq_ids/ ) {
        $line = <VCF>;
    }
    if( !$line ) {
        # Not necessary an error, the provided interval does not contain
        # any SNP.
        print_msg("Parsing thread", $int_id, "[WARNING] No SNP found in interval $int_id.");
    } else {
        # 2.5) Reach the first line
        my @loc = split(/\t/, $line, 3);
        while( $line && $loc[1] < $int_data{$loc[0]}{'from'} ) {
            $line = <VCF>;
            @loc = split(/\t/, $line, 3);
            if( !exists($int_data{$loc[0]}) ) {
                last;
            }
        }

        # 2.6) Treat each line until the end of the interval (or of the file)
        # The following algorithm assumes that intervals on different sequences
        # are contiguous.
        chomp($line);
        my @smp = split(/\t/, $line);
        while( $line && exists($int_data{$smp[0]}) && $smp[1] <= $int_data{$smp[0]}{'to'} ) {
            # Prepare filter counter
            my $ng = 0; # Number of gapped samples
            my $ni = 0; # Number of samples below the "miaf"
            my $na = 0; # Number of samples above the "maaf"
            my @afs;
            foreach (1..$ns) {
                my @gt = split(/\:/, $smp[8+$_]);
                # The sample is considered gapped if GATK sets not genotype
                if( $gt[0] =~ /\.[\/\|]\./ ) {
                    push @afs, -1.0;
                    $ng++;
                } else {
                    my @ad = split(/,/, $gt[$iad]);
                    if( $ad[0]+$ad[1] == 0 ) {
                        push @afs, -1.0;
                        $ng++;
                    } else {
                        my $af = $ad[1] / ($ad[0] + $ad[1]);
                        push @afs, $af;
                        if( $af < $filters->{min_af} ) {
                            $ni++;
                        } elsif( $af > $filters->{n_max_af} ) {
                            $na++;
                        }
                    }
                }
            }

            # Treat filters
            my $flag = 0;
            if( $ng > 0 ) {
                # This is a gappy SNP
                $flag = $flag + $flags->{'gapped'};
                if ( $ng >= $th_ngap ) {
                    # The proportion of gapped samples is reached
                    $flag = $flag + $flags->{'n_gapped'};
                }
            }
            if( $ni == $ns ) {
                # All the samples have an AF < miaf
                $flag = $flag + $flags->{'all_miaf'};
            }
            if( $ni >= $th_nmiaf ) {
                # At least N samples have an AF < miaf
                $flag = $flag + $flags->{'n_miaf'};
            }
            if( $na >= $th_nmaaf ) {
                # At least N samples have an AF > maaf
                 $flag = $flag + $flags->{'n_maaf'};
            }

            # Store data
            push @{$data->[$int_id]->{status}}, $flag;
            push @{$data->[$int_id]->{seq_id}}, $smp[0];
            push @{$data->[$int_id]->{location}}, $smp[1];
            push @{$data->[$int_id]->{af}}, join("\t", @afs);

            # Go to the next line
            $line = <VCF>;
            if( $line ) {
                chomp($line);
                @smp = split(/\t/, $line);
            }
        }
    }
    print_msg("Parsing thread", $int_id, "Parsing of interval $int_id finished.");
}

return 1;
