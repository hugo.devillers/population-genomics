package PopGeno::reference;

require Exporter;
use strict;
use warnings;

our @ISA    = qw(Exporter);
our @EXPORT = qw(check_reference);

# Check if the reference genome exists and if it is indexed
# Return the dictionnary path
sub check_reference {
    my $ref_genome = shift;
    if( !defined $ref_genome ) {
        die 'You must provide a reference genome file (in FASTA).';
    }
    if( !-f $ref_genome ) {
        die 'The reference genome file does not exist.';
    }
    if( !-f $ref_genome.'.fai') {
        die 'The reference genome must be indexed. Please, use samtools faidx.';
    }
    my $ref_dict = $ref_genome;
    $ref_dict =~ s/\.\w+$/\.dict/;
    if( !-f $ref_dict ) {
        die 'Missing sequence dictionnary (GATK CreateSequenceDictionary).';
    }
    return($ref_dict);
}

return 1;