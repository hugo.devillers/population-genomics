package PopGeno::bam;

require Exporter;
use strict;
use warnings;

our @ISA    = qw(Exporter);
our @EXPORT = qw(check_bam);

# Check if the provided BAM file exists and if it is index
# Return the index path
sub check_bam {
    my $input_bam = shift;

    # Check the bam file
    if( !defined $input_bam ) {
        die 'You must provide an input BAM file.';
    }

    # Check if bam file is indexed
    my $input_bai = $input_bam;
    $input_bai =~ s/bam$/bai/; # GATK like BAI
    if( !-f $input_bai ) {
        $input_bai = $input_bam.'.bai'; # Samtools like BAI
        if( !-f $input_bai ) {
            die 'Your BAM '.$input_bam.' file must be indexed.';
        }
    }
    return($input_bai);
}