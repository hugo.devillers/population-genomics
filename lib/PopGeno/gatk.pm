package PopGeno::gatk;

require Exporter;
use strict;
use warnings;
use PopGeno::message qw(print_msg);

our @ISA    = qw(Exporter);
our @EXPORT = qw(check_version gather haplotypecaller combinegvcfs genotypegvcfs biallelicsnphardfilter);

# Check that GATK binary exists and get the version
# Return the complete path of the binary
sub check_version {
    my $gatk = shift;
    if( !$gatk || $gatk eq "" ) {
        $gatk = `which gatk`;
        if( !$gatk ) {
            die 'Failed to find gatk binary, please provide it with argument --gatk-bin.';
        }
    }
    
    # Check the version
    my $gatk_version = `$gatk --version 2>/dev/null`;
    if( $gatk_version =~ /\(GATK\) v([\d\.]+)/ ) {
        print_msg("Main thread", 1, "Working with GATK v$1");
    } else {
        die 'Failed to find gatk binary, please provide it with argument --gatk-bin.'
    }

    return($gatk);
}

sub gather {
    my $gatk     = shift;
    my $files    = shift;
    my $base_out = shift;
    my $gvcf     = shift;

    
    print_msg('Main thread', 1, 'Gathering g.vcf files splitted by intervals...');
    my $files_in = '';
    foreach (@{$files}) { $files_in .= "-I $_ "; }
    if( $gvcf ) {
        my $cmd = `$gatk GatherVcfs -O $base_out.g.vcf $files_in 1>GatherVcfs.log 2>&1`;
    } else {
        my $cmd = `$gatk GatherVcfs -O $base_out.vcf $files_in 1>GatherVcfs.log 2>&1`;
    }
    print_msg('Main thread', 1, 'Gathering completed.');
}

# Launch an HaplotypeCaller on a given interval
sub haplotypecaller {
    my $gatk         = shift;
    my $interval_dir = shift;
    my $interval_id  = shift;
    my $bam_file     = shift;
    my $ref_genome   = shift;
    my $base_out     = shift;
    my $sample_ploidy = shift;
    my $thn          = "Calling thread";
    my $outputs      = shift;
    my $logs         = shift;
    my $gvcf         = shift;
    my $output       = '';
    my $outlog       = '';

    my $intervals = $interval_dir.'/'.sprintf("%04d", $interval_id)."-scattered.interval_list";
    print_msg($thn, $interval_id+1, "Starting HaplotypeCaller on interval $intervals...");
    if( $gvcf ) {
        $output = $base_out.sprintf("%03d", $interval_id+1).'.g.vcf';
        $outlog = $base_out.sprintf("%03d", $interval_id+1).'.log';
        my $cmd    = `$gatk --java-options "-Xmx16G -XX:ConcGCThreads=1 -XX:ParallelGCThreads=1" HaplotypeCaller -R $ref_genome -I $bam_file -L $intervals --sample-ploidy $sample_ploidy -O $output -ERC GVCF 1>$outlog 2>&1`;
    } else {
        $output = $base_out.sprintf("%03d", $interval_id+1).'.vcf';
        $outlog = $base_out.sprintf("%03d", $interval_id+1).'.log';
        my $cmd    = `$gatk --java-options "-Xmx16G -XX:ConcGCThreads=1 -XX:ParallelGCThreads=1" HaplotypeCaller -R $ref_genome -I $bam_file -L $intervals --sample-ploidy $sample_ploidy -O $output 1>$outlog 2>&1`;
    }
    print_msg($thn, $interval_id+1, "HaplotypeCaller on interval $intervals finished.");
    $outputs->[$interval_id] = $output;
    $logs->[$interval_id] = $outlog;
}

# Launch a CombineGVCFs on a given interval
sub combinegvcfs {
    my $gatk         = shift;
    my $interval_dir = shift;
    my $interval_id  = shift;
    my $gvcf_files   = shift;
    my $ref_genome   = shift;
    my $base_out     = shift;
    my $outputs      = shift;
    my $logs         = shift;
    my $thn          = "Combining thread";

    my $gvcf_input = '';
    foreach (@{$gvcf_files}) { $gvcf_input .= "-V $_ "; }
    my $intervals = $interval_dir.'/'.sprintf("%04d", $interval_id)."-scattered.interval_list";
    print_msg($thn, $interval_id+1, "Starting CombineGVCFs on interval $intervals...");
    my $output = $base_out.sprintf("%03d", $interval_id+1).'.g.vcf';
    my $outlog = $base_out.sprintf("%03d", $interval_id+1).'.log';
    my $cmd = `$gatk --java-options "-Xmx8G -XX:+UseParallelGC -XX:ParallelGCThreads=1" CombineGVCFs $gvcf_input -R $ref_genome -L $intervals -O $output 1>$outlog 2>&1`;
    print_msg($thn, $interval_id+1, "CombineGVCFs on interval $intervals finished.");
    $outputs->[$interval_id] = $output;
    $logs->[$interval_id] = $outlog;
}

sub genotypegvcfs {
    my $gatk         = shift;
    my $interval_dir = shift;
    my $interval_id  = shift;
    my $gvcf         = shift;
    my $ref_genome   = shift;
    my $base_out     = shift;
    my $outputs      = shift;
    my $logs         = shift;
    my $thn          = "Genotyping thread";

    my $intervals = $interval_dir.'/'.sprintf("%04d", $interval_id)."-scattered.interval_list";
    print_msg($thn, $interval_id+1, "Starting GenotypeGVCFs on interval $intervals...");
    my $output = $base_out.sprintf("%03d", $interval_id+1).'.g.vcf';
    my $outlog = $base_out.sprintf("%03d", $interval_id+1).'.log';
    my $cmd = `$gatk --java-options "-Xmx8G -XX:+UseParallelGC -XX:ParallelGCThreads=1" GenotypeGVCFs -V $gvcf -R $ref_genome -L $intervals -O $output 1>$outlog 2>&1`;
    print_msg($thn, $interval_id+1, "GenotypeGVCFs on interval $intervals finished.");
    $outputs->[$interval_id] = $output;
    $logs->[$interval_id] = $outlog;
}

sub biallelicsnphardfilter {
    my $gatk         = shift;
    my $interval_dir = shift;
    my $interval_id  = shift;
    my $vcf          = shift;
    my $ref_genome   = shift;
    my $base_out     = shift;
    my $outputs      = shift;
    my $logs         = shift;
    my $thn          = "Filtering thread";

    my $intervals = $interval_dir.'/'.sprintf("%04d", $interval_id)."-scattered.interval_list";
    print_msg($thn, $interval_id+1, "Starting hard filtering of biallelic SNP on interval $intervals...");
    my $outlog = $base_out.sprintf("%03d", $interval_id+1).'.log';
    
    # Step one extract biallelic SNPs
    my $out1 = $base_out.sprintf("%03d", $interval_id+1).'.bialsnp.vcf';
    my $cmd = `$gatk --java-options "-Xmx8G -XX:+UseParallelGC -XX:ParallelGCThreads=1" SelectVariants -V $vcf -R $ref_genome -L $intervals -O $out1 --select-type-to-include SNP --restrict-alleles-to BIALLELIC 1>$outlog 2>&1`;

    # Step two apply hard filters
    my $out2 = $base_out.sprintf("%03d", $interval_id+1).'.hardf.vcf';
    $cmd = `$gatk --java-options "-Xmx8G -XX:+UseParallelGC -XX:ParallelGCThreads=1" VariantFiltration -V $out1 -R $ref_genome -L $intervals -O $out2 -filter "QD < 2.0" --filter-name Low_QD -filter "FS > 60.0" --filter-name High_FS -filter "SOR > 3.0" --filter-name High_SOR -filter "MQ < 40.0" --filter-name Low_MQ -filter "MQRankSum < -2.5 || MQRankSum > 2.5" --filter-name "Bad_MQRS" -filter "ReadPosRankSum < -8.0 || ReadPosRankSum > 8.0" --filter-name Bad_RPRS 1>$outlog 2>&1`;
    unlink $out1;
    unlink $out1.'.idx';

    # Step three delete filtered SNPs
    my $out3 = $base_out.sprintf("%03d", $interval_id+1).'.vcf';
    $cmd = `$gatk --java-options "-Xmx8G -XX:+UseParallelGC -XX:ParallelGCThreads=1" SelectVariants -V $out2 -R $ref_genome -L $intervals -O $out3 --exclude-filtered 1>$outlog 2>&1`;
    unlink $out2;
    unlink $out2.'.idx';

    print_msg($thn, $interval_id+1, "Hard filtering of biallelic SNP on interval $intervals finished.");
    $outputs->[$interval_id] = $out3;
    $logs->[$interval_id] = $outlog;
}

return 1;
